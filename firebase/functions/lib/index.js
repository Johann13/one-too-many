"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);
// // Start writing Firebase Functions
// // https://firebase.google.com/functions/write-firebase-functions
//
//  export const helloWorld = functions.https
//      .onRequest((request, response) => {
//          response.send("Hello from Firebase!");
//          console.log("Test");
//      });
const DrunkRooms = "DrunkRooms";
const Users = "Users";
const Debts = "Debts";
const InviteRequest = "InviteRequest";
exports.createUser = functions.auth.user()
    .onCreate(event => {
    const user = event.data;
    const userName = user.displayName || null;
    const userEmail = user.email || null;
    return admin.firestore().collection(Users)
        .doc(user.uid).set({
        'name': userName,
        'email': userEmail,
        'id': user.uid,
    });
});
exports.deleteUser = functions.auth.user()
    .onDelete(event => {
    return admin.firestore()
        .collection(Users)
        .doc(event.data.uid)
        .collection(DrunkRooms)
        .get()
        .then(roomsData => {
        const batch = admin.firestore().batch();
        roomsData.forEach(roomData => {
            batch.delete(admin.firestore()
                .collection(DrunkRooms)
                .doc(roomData.id)
                .collection(Users)
                .doc(event.data.uid));
            batch.delete(roomData.ref);
        });
        batch.delete(admin.firestore()
            .collection(Users)
            .doc(event.data.uid));
        return batch.commit();
    });
});
exports.addRoom = functions.firestore
    .document(Users + "/{uId}/" + DrunkRooms + "/{dId}")
    .onCreate(event => {
    const uId = event.params.uId;
    const dId = event.params.dId;
    const userPromise = admin.firestore()
        .collection(Users)
        .doc(uId)
        .get();
    const roomPromise = admin.firestore()
        .collection(Users).doc(uId)
        .collection(DrunkRooms).doc(dId)
        .get();
    return Promise.all([userPromise, roomPromise])
        .then(data => {
        const batch = admin.firestore().batch();
        batch.set(admin.firestore()
            .collection(DrunkRooms)
            .doc(dId), data[1].data(), { merge: true });
        batch.create(admin.firestore()
            .collection(DrunkRooms)
            .doc(dId)
            .collection(Users)
            .doc(uId), data[0].data());
        return batch.commit();
    });
});
exports.enterRoom = functions.firestore
    .document(DrunkRooms + "/{dId}/" + Users + "/{uId}")
    .onCreate(event => {
    const room = event.data.data();
    return admin.firestore()
        .collection(Users)
        .doc(event.params.uId)
        .get()
        .then(userData => {
        const messagingToken = userData.get("messagingToken");
        const payload = {
            notification: {
                title: `You entered the ${room.name} title`,
                body: `You entered the ${room.name} room`,
            }
        };
        return admin.messaging().sendToDevice(messagingToken, payload)
            .then(function (response) {
            console.log("Successfully sent message:", response);
        })
            .catch(function (error) {
            console.log("Error sending message:", error);
        });
    });
});
exports.leaveRoom = functions.firestore
    .document(Users + "/{uId}/" + DrunkRooms + "/{dId}")
    .onDelete(event => {
    const uId = event.params.uId;
    const dId = event.params.dId;
    return admin.firestore()
        .collection(DrunkRooms)
        .doc(dId)
        .collection(Users)
        .doc(uId)
        .delete();
    ;
});
exports.inviteRequest = functions.firestore
    .document(InviteRequest + "/{id}")
    .onCreate(event => {
    const invite = event.data.data();
    //const hostId = invite.hostId;
    const roomId = invite.roomId;
    const invitedUserId = invite.invitedUserId;
    const invitedUserPromise = admin.firestore()
        .collection(Users).doc(invitedUserId).get();
    const roomPromise = admin.firestore()
        .collection(DrunkRooms).doc(roomId).get();
    return Promise.all([invitedUserPromise, roomPromise])
        .then(result => {
        const user = result[0].data();
        const room = result[1].data();
        const batch = admin.firestore().batch();
        batch.create(admin.firestore()
            .collection(Users)
            .doc(invitedUserId)
            .collection(DrunkRooms)
            .doc(roomId), room);
        batch.create(admin.firestore()
            .collection(DrunkRooms)
            .doc(roomId)
            .collection(Users)
            .doc(invitedUserId), user);
        return batch.commit().then(b => {
            return admin.firestore().collection(InviteRequest)
                .doc(event.params.id).delete();
        });
    });
});
exports.addDebt = functions.firestore
    .document(DrunkRooms + "/{drunkId}/" + Debts + "/{debtId}")
    .onCreate(event => {
    const debt = event.data.data();
    const price = debt.drink.price;
    const userBuyerPromise = admin.firestore()
        .collection(DrunkRooms)
        .doc(event.params.drunkId)
        .collection(Users)
        .doc(debt.userBuyer.id).get();
    const userReceiverPromise = admin.firestore()
        .collection(DrunkRooms)
        .doc(event.params.drunkId)
        .collection(Users)
        .doc(debt.userReceiver.id).get();
    return Promise.all([userBuyerPromise, userReceiverPromise])
        .then(result => {
        const userBuyerRef = result[0];
        const userReceiverRef = result[1];
        const buyerDebt = userBuyerRef.get("debt");
        const receiveDebt = userReceiverRef.get("debt");
        const batch = admin.firestore().batch();
        batch.set(admin.firestore().collection("DrunkRooms")
            .doc(event.params.drunkId)
            .collection("Users")
            .doc(userBuyerRef.id)
            .collection("Debts")
            .doc(debt.id), debt);
        batch.set(admin.firestore()
            .collection("DrunkRooms")
            .doc(event.params.drunkId)
            .collection("Users")
            .doc(userBuyerRef.id), { debt: buyerDebt + price }, { merge: true });
        batch.set(admin.firestore()
            .collection("DrunkRooms")
            .doc(event.params.drunkId)
            .collection("Users")
            .doc(userReceiverRef.id)
            .collection("Debts")
            .doc(debt.id), debt);
        batch.set(admin.firestore().collection("DrunkRooms")
            .doc(event.params.drunkId)
            .collection("Users")
            .doc(userReceiverRef.id), { debt: receiveDebt - price }, { merge: true });
        return batch.commit();
    });
});
exports.deleteDebt = functions.firestore
    .document(DrunkRooms + "/{drunkId}/" + Debts + "/{debtId}")
    .onDelete(event => {
    const debt = event.data.data();
    const price = debt.drink.price;
    const userBuyerPromise = admin.firestore()
        .collection(DrunkRooms)
        .doc(event.params.drunkId)
        .collection(Users)
        .doc(debt.userBuyer.id).get();
    const userReceiverPromise = admin.firestore()
        .collection(DrunkRooms)
        .doc(event.params.drunkId)
        .collection(Users)
        .doc(debt.userReceiver.id).get();
    return Promise.all([userBuyerPromise, userReceiverPromise])
        .then(result => {
        const userBuyerRef = result[0];
        const userReceiverRef = result[1];
        const buyerDebt = userBuyerRef.get("debt");
        const receiveDebt = userReceiverRef.get("debt");
        const batch = admin.firestore().batch();
        batch.delete(admin.firestore()
            .collection("DrunkRooms")
            .doc(event.params.drunkId)
            .collection("Users")
            .doc(userBuyerRef.id)
            .collection("Debts")
            .doc(debt.id));
        batch.set(admin.firestore().collection("DrunkRooms")
            .doc(event.params.drunkId)
            .collection("Users")
            .doc(userBuyerRef.id), { debt: buyerDebt - price }, { merge: true });
        batch.delete(admin.firestore()
            .collection("DrunkRooms")
            .doc(event.params.drunkId)
            .collection("Users")
            .doc(userReceiverRef.id)
            .collection("Debts")
            .doc(debt.id));
        batch.set(admin.firestore().collection("DrunkRooms")
            .doc(event.params.drunkId)
            .collection("Users")
            .doc(userReceiverRef.id), { debt: receiveDebt + price }, { merge: true });
        return batch.commit();
    });
});
//# sourceMappingURL=index.js.map