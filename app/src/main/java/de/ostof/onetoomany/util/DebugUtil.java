package de.ostof.onetoomany.util;

/**
 * Created by johan on 29.08.2017.
 */

public class DebugUtil {

    /*
    public static List<Subject> getSubjectDebugList() {
        int[] colors = {Color.GREEN, Color.rgb(128, 0, 128), Color.BLUE
                , Color.rgb(255, 165, 0), Color.YELLOW, Color.CYAN,
                Color.RED, Color.GRAY};
        String[] names = {"Biologie", "Mathe", "Englisch",
                "Geschichte", "Politik", "Informatik",
                "Deutsch", "Religion"
        };
        String[] shorts = {"Bio", "Mat", "Eng",
                "Ges", "Pol", "Inf",
                "Deu", "Rel"
        };
        ArrayList<Subject> subjects
                = new ArrayList<>();
        for (int i = 0; i < colors.length; i++) {
            Subject subject = new Subject();
            subject.setColor(colors[i]);
            subject.setName(names[i]);
            subject.setShortName(shorts[i]);
            subjects.add(subject);
        }
        return subjects;
    }

    public static List<Grade> getGradeDebugList() {
        ArrayList<Grade> gradeList = new ArrayList<>();
        Random random = new Random();
        TagRepo tagRepo = new TagRepo();
        SubjectRepo subjectRepo = new SubjectRepo();
        GradeSystemRepo gradeSystemRepo = new GradeSystemRepo();
        GradeSystem gradeSystem = gradeSystemRepo.getCurrentGradeSystem();
        List<GradeSystemItem> gradeSystemItemList = gradeSystem.getGradeItemList();
        RealmResults<Subject> subjectList = subjectRepo.getAll();
        RealmResults<Tag> tagList = tagRepo.getAll();
        for (int i = 0; i < 30; i++) {
            Grade grade = new Grade();

            int gradeItemPos = -1;
            if (!gradeSystemItemList.isEmpty()) {
                gradeItemPos = random.nextInt(gradeSystemItemList.size());
            }
            int subjectPos = -1;
            if (!subjectList.isEmpty()) {
                subjectPos = random.nextInt(subjectList.size());
            }
            int tagPos = -1;
            if (!tagList.isEmpty()) {
                tagPos = random.nextInt(tagList.size());
            }

            if (gradeItemPos > -1) {
                grade.setValues(gradeSystemItemList.get(gradeItemPos));
            }
            if (subjectPos > -1) {
                grade.setSubject(subjectRepo.fromRealm(subjectList.get(subjectPos)));
            }
            if (tagPos > -1) {
                grade.setTag(tagRepo.fromRealm(tagList.get(tagPos)));
            }
            grade.setDate(new Date());
            grade.setName("Test grade " + (i > 9 ? i : "0" + i));
            gradeList.add(grade);
        }
        return gradeList;
    }*/

}
