package de.ostof.onetoomany.util;

import android.content.res.Resources;

/**
 * Created by Johann on 08.03.2017.
 * Just calculate dps to pxs and reverse.
 */
@SuppressWarnings("unused")
public class DisplayUtil {

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(float px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(float dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
