package de.ostof.onetoomany.services;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import timber.log.Timber;

/**
 * Created by Johann on 08.01.2018.
 */

public class MFirebaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Timber.tag("MessagingService");
        Timber.d("onMessageReceived");
        if (remoteMessage != null) {
            String title = remoteMessage.getNotification().getTitle();
            String msg = remoteMessage.getNotification().getBody();
            NotificationManagerCompat notificationManagerCompat
                    = getSystemService(NotificationManagerCompat.class);

            if (notificationManagerCompat != null) {
                notificationManagerCompat.notify(3,
                        new NotificationCompat.Builder(this, "Channel1")
                                .setContentTitle(title)
                                .setContentText(msg)
                                .build());
            }
        }
    }

}
