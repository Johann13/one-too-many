package de.ostof.onetoomany.startup;

import android.arch.lifecycle.ViewModel;
import android.content.Intent;

import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import de.ostof.onetoomany.repo.Repo;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static de.ostof.onetoomany.startup.SplashActivity.RC_SIGN_IN;

/**
 * Created by Johann on 03.01.2018.
 */

public class SplashViewModel extends ViewModel {

    private SplashInteraction interaction;
    private Repo repo;

    public SplashViewModel() {
        repo = new Repo();
    }


    private boolean setup() {
        if (repo.isUserLoggedIn()) {
            // already signed in
            repo.refreshRegistration();
            return true;
        } else {
            // not signed in
            return false;
        }
    }

    public void start() {
        if (interaction != null) {
            if (setup()) {
                interaction.startMainActivity();
            } else {
                interaction.startLoginActivity();
            }
        }
    }

    public void setInteraction(SplashInteraction interaction) {
        this.interaction = interaction;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                repo.sendRegistration(FirebaseInstanceId.getInstance().getToken());
                interaction.startMainActivity();
                // ...
            } else {
                // Sign in failed, check response for error code
                // ...
                if (response != null) {
                    Timber.e("Error %s", response.getErrorCode());
                }
            }
        }
    }
}
