package de.ostof.onetoomany.startup;

/**
 * Created by Johann on 03.01.2018.
 */

public interface SplashInteraction {

    void startMainActivity();

    void startLoginActivity();

}
