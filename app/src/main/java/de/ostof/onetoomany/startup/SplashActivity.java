package de.ostof.onetoomany.startup;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.firebase.ui.auth.AuthUI;

import java.util.Arrays;

import de.ostof.onetoomany.MainActivity;

public class SplashActivity extends AppCompatActivity implements SplashInteraction {

    public static final int RC_SIGN_IN = 221;
    private SplashViewModel viewModel;

    @Override
    public void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void startLoginActivity() {
        AuthUI.IdpConfig googleIdp
                = new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER)
                .build();
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setIsSmartLockEnabled(false, true)
                        .setAvailableProviders(Arrays.asList(
                                new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER)
                                        .build(),
                                googleIdp))
                        .build(),
                RC_SIGN_IN);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
        viewModel.setInteraction(this);
        if (savedInstanceState == null) {
            viewModel.start();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }
}
