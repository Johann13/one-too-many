package de.ostof.onetoomany.repo;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import de.ostof.onetoomany.model.Debt;
import de.ostof.onetoomany.model.Drink;
import de.ostof.onetoomany.model.DrunkRoom;
import de.ostof.onetoomany.model.Invite;
import de.ostof.onetoomany.model.User;
import timber.log.Timber;

/**
 * Created by Johann on 05.01.2018.
 */

public class Repo {
    private FirebaseFirestore firestore;
    private FirebaseAuth auth;
    private FirebaseDatabase database;

    private static final String DRUNK_ROOMS = "DrunkRooms";
    private static final String USERS = "Users";
    private static final String DRINKS = "Drinks";
    private static final String DEBTS = "Debts";

    public Repo() {
        firestore = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
    }

    public boolean isUserLoggedIn() {
        return auth.getCurrentUser() != null;
    }

    @Nullable
    public FirebaseUser getFirebaseUser() {
        return auth.getCurrentUser();
    }

    @Nullable
    public User getUser() {
        FirebaseUser firebaseUser = getFirebaseUser();
        if (firebaseUser != null) {
            User user = new User();
            user.setId(firebaseUser.getUid());
            user.setName(firebaseUser.getDisplayName());
            user.setEmail(firebaseUser.getEmail());
            return user;
        } else {
            return null;
        }
    }

    public void addUser(User user,
                        @NonNull OnCompleteListener<Void> onCompleteListener,
                        @NonNull OnFailureListener onFailureListener) {
        firestore.collection(USERS)
                .document(user.getId())
                .set(user)
                .addOnCompleteListener(onCompleteListener)
                .addOnFailureListener(onFailureListener);
    }

    public void addDrunkRoom(@NonNull DrunkRoom drunkRoom,
                             @NonNull OnComplete<DrunkRoom> onComplete) {
        addDrunkRoom(drunkRoom, onComplete, Timber::e);
    }

    public void addDrunkRoom(DrunkRoom drunkRoom,
                             @NonNull OnComplete<DrunkRoom> onComplete,
                             @NonNull OnFailureListener onFailureListener) {
        addDrunkRoom(drunkRoom, (OnCompleteListener<Void>) task -> {
            if (task.isSuccessful()) {
                onComplete.onComplete(drunkRoom);
                FirebaseMessaging.getInstance().subscribeToTopic(drunkRoom.getId());
            }
        }, onFailureListener);
    }

    public void addDrunkRoom(DrunkRoom drunkRoom,
                             @NonNull OnCompleteListener<Void> onCompleteListener,
                             @NonNull OnFailureListener onFailureListener) {
        User user = getUser();
        if (user == null) {
            try {
                throw new Exception("There should be a user?");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        drunkRoom.setAdminId(user.getId());
        /*
        firestore.collection("DrunkRooms")
                .add(drunkRoom)
                .addOnCompleteListener(onCompleteListener)
                .addOnFailureListener(onFailureListener);*/
        DocumentReference ref = firestore.collection(USERS)
                .document(user.getId())
                .collection(DRUNK_ROOMS).document();
        drunkRoom.setId(ref.getId());
        ref.set(drunkRoom)
                .addOnCompleteListener(onCompleteListener)
                .addOnFailureListener(onFailureListener);
    }

    public void deleteDrunkRoom(@NonNull DrunkRoom drunkRoom,
                                @NonNull OnComplete<DrunkRoom> onComplete) {
        User user = getUser();
        if (user == null) {
            return;
        }
        firestore.collection(USERS)
                .document(user.getId())
                .collection(DRUNK_ROOMS)
                .document(drunkRoom.getId())
                .delete()
                .addOnSuccessListener(aVoid -> {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(drunkRoom.getId());
                    onComplete.onComplete(drunkRoom);
                }).addOnFailureListener(Timber::e);
    }

    public Query getMyDrunkRooms() {
        User user = getUser();
        if (user == null) {
            return null;
        }
        return firestore.collection(USERS)
                .document(user.getId())
                .collection(DRUNK_ROOMS)
                .orderBy("name");
    }

    public Query getUsersFromDrunkRoom(String roomId) {
        return firestore.collection(DRUNK_ROOMS)
                .document(roomId)
                .collection(USERS)
                .orderBy("name");
    }

    public Query getUsersFromDrunkRoomWithoutMe(String roomId) {
        return firestore.collection(DRUNK_ROOMS)
                .document(roomId)
                .collection(USERS)
                .orderBy("name");
    }

    public Query getDrinksFromDrunkRoom(String roomId) {
        return firestore.collection(DRUNK_ROOMS)
                .document(roomId)
                .collection(DRINKS)
                .orderBy("name");
    }

    public DocumentReference get(@NonNull String id) {
        return firestore.collection(DRUNK_ROOMS).document(id);

    }

    public void invite(Invite invite, @NonNull OnComplete<Invite> onComplete) {
        invite.setHostId(getFirebaseUser().getUid());
        Timber.d(invite.toString());
        firestore.collection("InviteRequest")
                .add(invite)
                .addOnSuccessListener(documentReference -> onComplete.onComplete(invite))
                .addOnFailureListener(Timber::e);
    }

    public void addDrink(String roomId, String name, float price, @NonNull OnComplete<Drink> onComplete) {
        DocumentReference ref = firestore.collection(DRUNK_ROOMS)
                .document(roomId)
                .collection(DRINKS).document();
        Drink drink = new Drink();
        drink.setId(ref.getId());
        drink.setName(name);
        drink.setPrice(price);
        ref.set(drink)
                .addOnFailureListener(Timber::e)
                .addOnSuccessListener(aVoid -> onComplete.onComplete(drink));
    }

    public void addDebt(@NonNull String roomId,
                        @NonNull User userBuyer,
                        @NonNull User userReceiver,
                        @NonNull Drink drink,
                        @NonNull OnComplete<Debt> onComplete) {

        DocumentReference ref = firestore.collection(DRUNK_ROOMS)
                .document(roomId)
                .collection(DEBTS).document();

        Debt debt = new Debt();
        debt.setId(ref.getId());
        debt.setUserBuyer(userBuyer);
        debt.setUserReceiver(userReceiver);
        debt.setDrink(drink);

        ref.set(debt)
                .addOnFailureListener(Timber::e)
                .addOnSuccessListener(aVoid -> onComplete.onComplete(debt));

    }

    public Query getMyDebts(String drunkRoomId) {

        User user = getUser();
        if (user == null) {
            return null;
        }
        return firestore
                .collection(DRUNK_ROOMS)
                .document(drunkRoomId)
                .collection(USERS)
                .document(user.getId())
                .collection(DEBTS).orderBy("drink.price");
    }

    public Query getMyDebtsBought(String drunkRoomId) {
        User user = getUser();
        if (user == null) {
            return null;
        }
        return getMyDebts(drunkRoomId)
                .whereEqualTo("userBuyer.id", user.getId())
                .orderBy("userReceiver.name");
    }

    public Query getMyDebtsGot(String drunkRoomId) {
        User user = getUser();
        if (user == null) {
            return null;
        }
        return getMyDebts(drunkRoomId)
                .whereEqualTo("userReceiver.id", user.getId())
                .orderBy("userBuyer.name");

    }

    public interface OnComplete<T> {
        void onComplete(T item);
    }

    public void refreshRegistration() {
        sendRegistration(FirebaseInstanceId.getInstance().getToken());
    }

    public void sendRegistration(@Nullable String messagingToken) {
        User user = getUser();
        if (user != null) {
            if (messagingToken != null) {
                user.setMessagingToken(messagingToken);
                firestore.collection("Users")
                        .document(user.getId())
                        .set(user);
            }
        }
    }

}
