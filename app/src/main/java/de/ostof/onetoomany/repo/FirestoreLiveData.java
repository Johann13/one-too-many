package de.ostof.onetoomany.repo;

import android.arch.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import timber.log.Timber;

/**
 * Created by Johann on 06.01.2018.
 */

public class FirestoreLiveData extends LiveData<DocumentSnapshot> {
    private static final String LOG_TAG = "FirestoreLiveData";

    private DocumentReference documentReference;
    private final MyValueEventListener listener = new MyValueEventListener();
    private ListenerRegistration registration;

    public FirestoreLiveData(){

    }

    public FirestoreLiveData(DocumentReference documentReference) {
        this.documentReference = documentReference;
    }

    public void setDocumentReference(DocumentReference documentReference) {
        this.documentReference = documentReference;
    }

    @Override
    protected void onActive() {
        Timber.d("onActive");
        registration = documentReference.addSnapshotListener(listener);
    }

    @Override
    protected void onInactive() {
        Timber.d("onInactive");
        registration.remove();
    }

    private class MyValueEventListener implements EventListener<DocumentSnapshot> {

        @Override
        public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
            if (e != null) {
                Timber.e(e);
                return;
            }

            if (documentSnapshot != null) {
                setValue(documentSnapshot);
            }

        }
    }

}
