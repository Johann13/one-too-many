package de.ostof.onetoomany.account;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import de.ostof.onetoomany.model.Invite;
import de.ostof.onetoomany.repo.Repo;
import timber.log.Timber;

/**
 * Created by Johann on 10.01.2018.
 */

public class QRScannerViewModel extends ViewModel{

    private Repo repo;

    public QRScannerViewModel(){
        repo = new Repo();
    }

    public void invite(Invite invite,@NonNull Repo.OnComplete<Invite> onComplete){
        repo.invite(invite,onComplete);
    }

}
