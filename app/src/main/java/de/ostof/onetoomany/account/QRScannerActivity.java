package de.ostof.onetoomany.account;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.zxing.Result;

import de.ostof.onetoomany.model.Invite;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import timber.log.Timber;

public class QRScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;
    private QRScannerViewModel viewModel;
    private String roomId;

    @Override
    public void handleResult(Result result) {
        Timber.tag("QRScannerActivity");
        Timber.d(result.getText());
        Toast.makeText(this, "Fried " + result.getText() + " room " + roomId, Toast.LENGTH_LONG).show();
        finish();
        viewModel.invite(new Invite(result.getText(), roomId), item -> {
            Toast.makeText(this, "Fried added", Toast.LENGTH_SHORT).show();
            finish();
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(QRScannerViewModel.class);
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        roomId = getIntent().getStringExtra("roomId");
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        scannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();           // Stop camera on pause
    }
}
