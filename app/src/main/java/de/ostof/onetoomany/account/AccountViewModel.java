package de.ostof.onetoomany.account;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import de.ostof.onetoomany.model.User;
import de.ostof.onetoomany.repo.Repo;

/**
 * Created by Johann on 10.01.2018.
 */

public class AccountViewModel extends ViewModel {

    private Repo repo;
    private MutableLiveData<User> userLiveData;



    public AccountViewModel(){
        repo = new Repo();
    }

    public LiveData<User> getUserLiveData() {
        if(userLiveData==null){
            userLiveData = new MutableLiveData<>();
            userLiveData.setValue(repo.getUser());
        }
        return userLiveData;
    }
}
