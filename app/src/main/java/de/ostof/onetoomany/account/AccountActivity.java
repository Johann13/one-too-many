package de.ostof.onetoomany.account;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;

import net.glxn.qrgen.android.QRCode;

import butterknife.BindView;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.viewModelActivities.MenuViewModelActivity;
import de.ostof.onetoomany.startup.SplashActivity;

public class AccountActivity extends MenuViewModelActivity<AccountViewModel> {

    @BindView(R.id.texViewName)
    TextView texViewName;
    @BindView(R.id.texViewEmail)
    TextView texViewEmail;
    @BindView(R.id.imageViewQR)
    ImageView imageViewQR;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel.getUserLiveData().observe(this, user -> {
            if (user != null) {
                texViewName.setText(user.getName());
                texViewEmail.setText(user.getEmail());
                Bitmap myBitmap = QRCode.from(user.getId()).bitmap();
                imageViewQR.setImageBitmap(myBitmap);
            }
        });

    }

    @Override
    public String getTag() {
        return "AccountActivity";
    }

    @Override
    public int getToolbarTitle() {
        return R.string.account;
    }

    @Override
    public int getToolbarSubTitle() {
        return 0;
    }

    @Override
    public boolean shouldShowBackButton() {
        return true;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_account;
    }

    @NonNull
    @Override
    protected Class<AccountViewModel> getViewModelClass() {
        return AccountViewModel.class;
    }

    @Override
    protected int getMenuRes() {
        return R.menu.menu_account;
    }

    @Override
    public boolean onItemSelected(MenuItem menuItem, int id) {
        if (id == R.id.menu_logout) {
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(task -> {
                        // user is now signed out
                        startActivity(new Intent(this, SplashActivity.class));
                        finish();
                    });
            return true;
        }
        return false;
    }
}
