package de.ostof.onetoomany.model;

/**
 * Created by Johann on 10.01.2018.
 */

public class Invite {

    private String invitedUserId;
    private String hostId;
    private String roomId;

    public Invite(String userId, String roomId) {
        this.invitedUserId = userId;
        this.roomId = roomId;
    }

    public String getInvitedUserId() {
        return invitedUserId;
    }

    public void setInvitedUserId(String invitedUserId) {
        this.invitedUserId = invitedUserId;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    @Override
    public String toString() {
        return "RoomId: "+roomId+" user to invite "+ invitedUserId;
    }
}
