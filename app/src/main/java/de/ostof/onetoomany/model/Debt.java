package de.ostof.onetoomany.model;

/**
 * Created by Johann on 03.01.2018.
 */

public class Debt {

    private String id;
    private User userBuyer;
    private User userReceiver;
    private Drink drink;
    private int numberOfDrinks;

    public Debt() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUserBuyer() {
        return userBuyer;
    }

    public void setUserBuyer(User userBuyer) {
        this.userBuyer = userBuyer;
    }

    public User getUserReceiver() {
        return userReceiver;
    }

    public void setUserReceiver(User userReceiver) {
        this.userReceiver = userReceiver;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }

    public float getPrice() {
        return numberOfDrinks * drink.getPrice();
    }
}
