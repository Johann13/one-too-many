package de.ostof.onetoomany.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Johann on 03.01.2018.
 */

public class DrunkRoom {

    private String id;
    private String adminId;
    private String name;

    public DrunkRoom() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
