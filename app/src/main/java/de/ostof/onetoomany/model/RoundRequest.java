package de.ostof.onetoomany.model;

/**
 * Created by Johann on 20.01.2018.
 */

public class RoundRequest {

    private User user;
    private Drink drink;
    private DrunkRoom drunkRoom;

    public RoundRequest(){

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }

    public DrunkRoom getDrunkRoom() {
        return drunkRoom;
    }

    public void setDrunkRoom(DrunkRoom drunkRoom) {
        this.drunkRoom = drunkRoom;
    }
}
