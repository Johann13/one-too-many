package de.ostof.onetoomany.model;

/**
 * Created by Johann on 15.01.2018.
 */

public class Drink {

    private String id;
    private String name;
    private float price;

    public Drink(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
