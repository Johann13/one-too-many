package de.ostof.onetoomany;

import android.arch.lifecycle.ViewModel;

import de.ostof.onetoomany.model.User;
import de.ostof.onetoomany.repo.Repo;

/**
 * Created by Johann on 04.01.2018.
 */

public class MainViewModel extends ViewModel {

    private Repo repo;

    public MainViewModel(){
        repo = new Repo();
    }

    public User getUser() {
        return repo.getUser();
    }
}
