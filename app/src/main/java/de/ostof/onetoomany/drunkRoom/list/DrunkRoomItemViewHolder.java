package de.ostof.onetoomany.drunkRoom.list;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.recyclerViewAdapter.BaseViewHolder;
import de.ostof.onetoomany.model.DrunkRoom;

/**
 * Created by Johann on 04.01.2018.
 */

public class DrunkRoomItemViewHolder extends BaseViewHolder<DrunkRoom> {

    @BindView(R.id.textView)
    TextView textView;

    public DrunkRoomItemViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void setView(View view, DrunkRoom item, int pos) {
        textView.setText(item.getName());
    }
}
