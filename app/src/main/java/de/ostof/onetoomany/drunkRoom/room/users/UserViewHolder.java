package de.ostof.onetoomany.drunkRoom.room.users;

import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;

import butterknife.BindView;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.recyclerViewAdapter.BaseViewHolder;
import de.ostof.onetoomany.model.User;

/**
 * Created by Johann on 14.01.2018.
 */

public class UserViewHolder extends BaseViewHolder<User> {

    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.textViewDebt)
    TextView textViewDebt;

    public UserViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void setView(View view, User item, int pos) {
        if (item.getName() != null) {
            textViewName.setText(item.getName());
        } else {
            textViewName.setText(item.getEmail());
        }
        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
        textViewDebt.setText(defaultFormat.format(item.getDebt()));
    }
}
