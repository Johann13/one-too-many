package de.ostof.onetoomany.drunkRoom.room.drinks;

import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;

import butterknife.BindView;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.recyclerViewAdapter.BaseViewHolder;
import de.ostof.onetoomany.model.Drink;

/**
 * Created by Johann on 15.01.2018.
 */

class DrinkViewHolder extends BaseViewHolder<Drink> {

    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.textViewPrice)
    TextView textViewPrice;

    public DrinkViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void setView(View view, Drink item, int pos) {
        textViewName.setText(item.getName());
        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
        textViewPrice.setText(defaultFormat.format(item.getPrice()));
    }
}
