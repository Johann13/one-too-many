package de.ostof.onetoomany.drunkRoom.room.users;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import butterknife.BindView;
import butterknife.OnClick;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.viewModelFragments.BaseViewModelFragment;
import de.ostof.onetoomany.drunkRoom.room.DrunkRoomViewModel;
import de.ostof.onetoomany.drunkRoom.room.drinks.DrinkAdapter;
import de.ostof.onetoomany.model.Drink;
import de.ostof.onetoomany.model.DrunkRoom;
import de.ostof.onetoomany.model.User;

/**
 * A simple {@link Fragment} subclass.
 */
public class DrunkRoomUserFragment extends BaseViewModelFragment<DrunkRoomViewModel> {

    private DrunkRoom drunkRoom;
    private User user;
    private UserAdapter userAdapter;

    @BindView(R.id.recyclerViewUsers)
    RecyclerView recyclerViewUsers;
    @BindView(R.id.fabRound)
    FloatingActionButton fabRound;


    public DrunkRoomUserFragment() {
        // Required empty public constructor
    }

    @Override
    public void initView(@NonNull View view, @Nullable Bundle savedInstanceState) {
        user = viewModel.getUser();
        viewModel.getDrunkRoom(
                getActivity().getIntent().getStringExtra("drunkRoomId"))
                .observe(this, drunkRoom -> {
                    if (drunkRoom == null) {
                        return;
                    }
                    this.drunkRoom = drunkRoom;
                    FirestoreRecyclerOptions<User> options
                            = new FirestoreRecyclerOptions.Builder<User>()
                            .setQuery(viewModel.getUsers(drunkRoom.getId()), User.class)
                            .setLifecycleOwner(this).build();
                    options.getSnapshots().remove(user);
                    userAdapter = new UserAdapter(options);
                    userAdapter.setOnItemClickListener((user, pos) -> {
                        addDebt(user);
                    });
                    recyclerViewUsers.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerViewUsers.setAdapter(userAdapter);

                });
    }

    private void addDebt(User user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        @SuppressLint("InflateParams") View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_pick_drink, null);
        builder.setView(view);
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> {
        });
        AlertDialog dialog = builder.create();
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        TextView textView = view.findViewById(R.id.textViewTitle);
        textView.setText("Which drink did " + user.getName() + " buy for you?");

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        FirestoreRecyclerOptions<Drink> options
                = new FirestoreRecyclerOptions.Builder<Drink>()
                .setQuery(viewModel.getDrinks(drunkRoom.getId()), Drink.class)
                .setLifecycleOwner(this).build();
        DrinkAdapter drinkAdapter = new DrinkAdapter(options);
        drinkAdapter.setOnItemClickListener((drink, pos) -> {

            viewModel.addDebt(drunkRoom.getId(),
                    user, this.user, drink,
                    debt -> {
                        dialog.dismiss();
                    });
        });
        recyclerView.setAdapter(drinkAdapter);
        dialog.show();
    }

    @OnClick(R.id.fabRound)
    void addRound() {

    }

    @NonNull
    @Override
    public String getFragmentTag() {
        return "DrunkRoomUserFragment";
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_drunk_room_user;
    }


    @NonNull
    @Override
    protected Class<DrunkRoomViewModel> getViewModelClass() {
        return DrunkRoomViewModel.class;
    }
}
