package de.ostof.onetoomany.drunkRoom.room.debts;

import android.support.annotation.NonNull;
import android.view.View;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.recyclerViewAdapter.BaseAdapter;
import de.ostof.onetoomany.model.Debt;

/**
 * Created by Johann on 18.01.2018.
 */

public class DebtAdapter extends BaseAdapter<Debt, DebtViewHolder> {
    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    private String userId;

    public DebtAdapter(String userId, @NonNull FirestoreRecyclerOptions<Debt> options) {
        super(options);
        this.userId = userId;
    }

    @NonNull
    @Override
    public DebtViewHolder onCreateViewHolder(View view) {
        return new DebtViewHolder(view, userId);
    }

    @Override
    public int getItemLayout() {
        return R.layout.item_debt;
    }
}
