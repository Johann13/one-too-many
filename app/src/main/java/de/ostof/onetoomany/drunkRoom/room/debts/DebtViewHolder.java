package de.ostof.onetoomany.drunkRoom.room.debts;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;

import butterknife.BindView;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.recyclerViewAdapter.BaseViewHolder;
import de.ostof.onetoomany.model.Debt;

/**
 * Created by Johann on 18.01.2018.
 */

class DebtViewHolder extends BaseViewHolder<Debt> {

    private String userId;

    @BindView(R.id.textView)
    TextView textView;


    public DebtViewHolder(View itemView) {
        super(itemView);
    }

    public DebtViewHolder(View view, String userId) {
        super(view);
        this.userId = userId;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setView(View view, Debt item, int pos) {
        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();

        if (item.getUserReceiver() == null) {

            if (item.getUserBuyer().getName().equals(userId)) {
                textView.setText("I bought all" +
                        " " + item.getDrink().getName() +
                        "\nfor " + defaultFormat.format(item.getDrink().getPrice()) + " each.");
            } else {
                textView.setText(item.getUserBuyer().getName() +
                        " bought all " + item.getDrink().getName() +
                        "\nfor " + defaultFormat.format(item.getDrink().getPrice()) + ".");
            }

        } else {
            if (item.getUserReceiver().getId().equals(userId)) {
                textView.setText(item.getUserBuyer().getName() +
                        " bought me " + item.getDrink().getName() +
                        "\nfor " + defaultFormat.format(item.getDrink().getPrice()) + ".");
            } else {
                textView.setText("I bought " + item.getUserReceiver().getName() +
                        " " + item.getDrink().getName() +
                        "\nfor " + defaultFormat.format(item.getDrink().getPrice()) + ".");
            }
        }

    }
}
