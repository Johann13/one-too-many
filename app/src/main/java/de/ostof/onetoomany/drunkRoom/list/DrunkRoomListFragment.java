package de.ostof.onetoomany.drunkRoom.list;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import java.util.Collections;

import butterknife.BindView;
import butterknife.OnClick;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.viewModelFragments.BaseViewModelFragment;
import de.ostof.onetoomany.drunkRoom.room.DrunkRoomActivity;
import de.ostof.onetoomany.model.DrunkRoom;

/**
 * Created by Johann on 03.01.2018.
 */

public class DrunkRoomListFragment extends BaseViewModelFragment<DrunkRoomListViewModel> {

    @BindView(R.id.recyclerViewDrunkRoom)
    RecyclerView recyclerViewDrunkRoom;

    @BindView(R.id.fabAddDrunkRoom)
    FloatingActionButton fabAddDrunkRoom;

    @Override
    public void initView(@NonNull View view, @Nullable Bundle savedInstanceState) {
        FirestoreRecyclerOptions<DrunkRoom> options
                = new FirestoreRecyclerOptions.Builder<DrunkRoom>()
                .setQuery(viewModel.getMyDrunkRooms(), DrunkRoom.class)
                .setLifecycleOwner(this).build();
        DrunkRoomAdapter adapter = new DrunkRoomAdapter(options);
        adapter.setOnItemClickListener((item, pos) -> {
            Intent intent = new Intent(getActivity(), DrunkRoomActivity.class);
            intent.putExtra("drunkRoomId", item.getId());
            startActivity(intent);
        });
        adapter.setOnItemLongClickListener((item, pos) -> {

            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Leave " + item.getName())
                    .setMessage("Do you want to leave " + item.getName() + "?")
                    .setPositiveButton("Yes leave!", (dialogInterface, i) ->
                            viewModel.deleteDrunkRoom(item, drunkRoom -> {
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1 && getActivity() != null) {
                                    ShortcutManager shortcutManager = getActivity().getSystemService(ShortcutManager.class);
                                    if (shortcutManager != null) {
                                        shortcutManager.removeDynamicShortcuts(Collections.singletonList(drunkRoom.getId()));
                                    }
                                }
                            }))
                    .setNegativeButton("No remain.", null)
                    .create();
            alertDialog.show();

        });
        recyclerViewDrunkRoom.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewDrunkRoom.setAdapter(adapter);
    }

    @NonNull
    @Override
    public String getFragmentTag() {
        return "DrunkRoomListFragment";
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_drunk_room_list;
    }

    @NonNull
    @Override
    protected Class<DrunkRoomListViewModel> getViewModelClass() {
        return DrunkRoomListViewModel.class;
    }

    @OnClick(R.id.fabAddDrunkRoom)
    void addDrunkRoomClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        @SuppressLint("InflateParams") View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_add_drunk_room, null);
        builder.setView(view);

        //builder.setTitle("Create a Drunkroom");
        TextInputLayout textInputLayout = view.findViewById(R.id.textInputLayout);

        builder.setPositiveButton("OK", null)
                .setNegativeButton("Cancel", (dialogInterface, i) -> {
                });
        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(dialogInterface ->
                dialog.getButton(DialogInterface.BUTTON_POSITIVE)
                        .setOnClickListener(v -> {
                            textInputLayout.setError(null);
                            if (textInputLayout.getEditText() != null &&
                                    !textInputLayout.getEditText().getText().toString().isEmpty()) {
                                addDrunkRoom(textInputLayout.getEditText().getText().toString());
                                dialog.dismiss();
                            } else {
                                textInputLayout.setError("Enter a name");
                            }
                        }));

        dialog.show();

    }

    private void addDrunkRoom(@NonNull String name) {
        viewModel.addDrunkRoom(name, drunkRoom -> {
            Toast.makeText(getActivity(), "Added drunkRoom " + drunkRoom.getName(), Toast.LENGTH_SHORT).show();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1 && getActivity() != null) {
                ShortcutManager shortcutManager = getActivity().getSystemService(ShortcutManager.class);
                if (shortcutManager != null) {
                    Intent intent = new Intent(getActivity(), DrunkRoomActivity.class);
                    intent.setAction(drunkRoom.getId());
                    intent.putExtra("drunkRoomId", drunkRoom.getId());
                    ShortcutInfo shortcut = new ShortcutInfo.Builder(getActivity(), drunkRoom.getId())
                            .setShortLabel(drunkRoom.getName())
                            .setLongLabel("Open " + drunkRoom.getName())
                            .setIntent(intent)
                            .build();
                    shortcutManager.addDynamicShortcuts(Collections.singletonList(shortcut));
                }
            }
        });
    }
}
