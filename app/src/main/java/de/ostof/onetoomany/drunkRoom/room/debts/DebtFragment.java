package de.ostof.onetoomany.drunkRoom.room.debts;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import butterknife.BindView;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.viewModelFragments.BaseViewModelFragment;
import de.ostof.onetoomany.drunkRoom.room.DrunkRoomViewModel;
import de.ostof.onetoomany.model.Debt;
import de.ostof.onetoomany.model.DrunkRoom;
import de.ostof.onetoomany.model.User;

/**
 * Created by Johann on 18.01.2018.
 */

public class DebtFragment extends BaseViewModelFragment<DrunkRoomViewModel> {

    private DrunkRoom drunkRoom;
    private User user;
    private DebtAdapter debtAdapterB, debtAdapterG;

    @BindView(R.id.recyclerViewDebtBought)
    RecyclerView recyclerViewDebtBought;

    @BindView(R.id.recyclerViewDebtGot)
    RecyclerView recyclerViewDebtGot;

    @Override
    public void initView(@NonNull View view, @Nullable Bundle savedInstanceState) {
        user = viewModel.getUser();
        viewModel.getDrunkRoom(
                getActivity().getIntent().getStringExtra("drunkRoomId"))
                .observe(this, drunkRoom -> {
                    if (drunkRoom == null) {
                        return;
                    }
                    this.drunkRoom = drunkRoom;
                    FirestoreRecyclerOptions<Debt> optionsB
                            = new FirestoreRecyclerOptions.Builder<Debt>()
                            .setQuery(viewModel.getMyDebtsBought(drunkRoom.getId()), Debt.class)
                            .setLifecycleOwner(this).build();
                    optionsB.getSnapshots().remove(user);
                    debtAdapterB = new DebtAdapter(user.getId(), optionsB);
                    debtAdapterB.setOnItemClickListener((user, pos) -> {

                    });
                    recyclerViewDebtBought.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerViewDebtBought.setAdapter(debtAdapterB);

                    FirestoreRecyclerOptions<Debt> optionsG
                            = new FirestoreRecyclerOptions.Builder<Debt>()
                            .setQuery(viewModel.getMyDebtsGot(drunkRoom.getId()), Debt.class)
                            .setLifecycleOwner(this).build();
                    optionsG.getSnapshots().remove(user);
                    debtAdapterG = new DebtAdapter(user.getId(), optionsG);
                    debtAdapterG.setOnItemClickListener((user, pos) -> {

                    });
                    recyclerViewDebtGot.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerViewDebtGot.setAdapter(debtAdapterG);
                });
    }

    @NonNull
    @Override
    public String getFragmentTag() {
        return "DebtFragment";
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_drunk_room_debt;
    }

    @NonNull
    @Override
    protected Class<DrunkRoomViewModel> getViewModelClass() {
        return DrunkRoomViewModel.class;
    }
}
