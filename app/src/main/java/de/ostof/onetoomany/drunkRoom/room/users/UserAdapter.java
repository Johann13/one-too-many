package de.ostof.onetoomany.drunkRoom.room.users;

import android.support.annotation.NonNull;
import android.view.View;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.recyclerViewAdapter.BaseAdapter;
import de.ostof.onetoomany.model.User;

/**
 * Created by Johann on 14.01.2018.
 */

public class UserAdapter extends BaseAdapter<User, UserViewHolder> {
    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */

    public UserAdapter(@NonNull FirestoreRecyclerOptions<User> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull UserViewHolder holder, int position, @NonNull User model) {
        super.onBindViewHolder(holder, position, model);
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(View view) {
        return new UserViewHolder(view);
    }

    @Override
    public int getItemLayout() {
        return R.layout.item_user;
    }
}
