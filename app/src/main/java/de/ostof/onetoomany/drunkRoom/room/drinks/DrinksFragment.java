package de.ostof.onetoomany.drunkRoom.room.drinks;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import butterknife.BindView;
import butterknife.OnClick;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.viewModelFragments.BaseViewModelFragment;
import de.ostof.onetoomany.drunkRoom.room.DrunkRoomViewModel;
import de.ostof.onetoomany.model.Drink;
import de.ostof.onetoomany.model.DrunkRoom;

/**
 * Created by Johann on 15.01.2018.
 */

public class DrinksFragment extends BaseViewModelFragment<DrunkRoomViewModel> {

    private DrunkRoom drunkRoom;
    private DrinkAdapter drinkAdapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    public void initView(@NonNull View view, @Nullable Bundle savedInstanceState) {

        viewModel.getDrunkRoom(
                getActivity().getIntent().getStringExtra("drunkRoomId"))
                .observe(this, drunkRoom -> {
                    if (drunkRoom == null) {
                        return;
                    }
                    this.drunkRoom = drunkRoom;
                    logD(drunkRoom.getId());

//                    viewModel.getDrinks(drunkRoom.getId())
//                            .addSnapshotListener(getActivity(), (documentSnapshots, e) -> {
//                                if(e!=null){
//                                    logError(e);
//                                    return;
//                                }
//                                for(DocumentSnapshot documentSnapshot : documentSnapshots){
//                                    Drink drink = documentSnapshot.toObject(Drink.class);
//                                    logD(drink.getName()+" "+drink.getPrice());
//                                }
//                            });

                    FirestoreRecyclerOptions<Drink> options
                            = new FirestoreRecyclerOptions.Builder<Drink>()
                            .setQuery(viewModel.getDrinks(drunkRoom.getId()), Drink.class)
                            .setLifecycleOwner(this).build();
                    drinkAdapter = new DrinkAdapter(options);
                    drinkAdapter.setOnItemClickListener((item, pos) -> {

                    });
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setAdapter(drinkAdapter);

                });
    }


    @OnClick(R.id.fabAdd)
    void addDrink() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        @SuppressLint("InflateParams") View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_add_drink, null);
        builder.setView(view);

        TextInputLayout textInputLayoutName = view.findViewById(R.id.textInputLayoutName);
        TextInputLayout textInputLayoutPrice = view.findViewById(R.id.textInputLayoutPrice);

        builder.setPositiveButton("OK", null)
                .setNegativeButton("Cancel", (dialogInterface, i) -> {
                });
        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(dialogInterface ->
                dialog.getButton(DialogInterface.BUTTON_POSITIVE)
                        .setOnClickListener(v -> {
                            textInputLayoutName.setError(null);
                            textInputLayoutPrice.setError(null);

                            if (textInputLayoutPrice.getEditText() == null ||
                                    textInputLayoutPrice.getEditText().getText().toString().isEmpty()) {
                                textInputLayoutPrice.setError("Enter a price");
                                return;
                            }


                            if (textInputLayoutName.getEditText() == null ||
                                    textInputLayoutName.getEditText().getText().toString().isEmpty()) {
                                textInputLayoutName.setError("Enter a name");
                                return;
                            }

                            viewModel.addDrink(drunkRoom.getId(),
                                    textInputLayoutName.getEditText().getText().toString(),
                                    Float.valueOf(textInputLayoutPrice.getEditText().getText().toString()),
                                    drink -> Toast.makeText(getActivity(), "Added " + drink.getName(), Toast.LENGTH_SHORT).show());

                            dialog.dismiss();
                        }));

        dialog.show();
    }


    @NonNull
    @Override
    public String getFragmentTag() {
        return "DrinksFragment";
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_drunk_room_drink;
    }

    @NonNull
    @Override
    protected Class<DrunkRoomViewModel> getViewModelClass() {
        return DrunkRoomViewModel.class;
    }
}
