package de.ostof.onetoomany.drunkRoom.list;

import android.support.annotation.NonNull;
import android.view.View;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.recyclerViewAdapter.BaseAdapter;
import de.ostof.onetoomany.model.DrunkRoom;

/**
 * Created by Johann on 04.01.2018.
 */

public class DrunkRoomAdapter extends BaseAdapter<DrunkRoom,DrunkRoomItemViewHolder> {
    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public DrunkRoomAdapter(@NonNull FirestoreRecyclerOptions<DrunkRoom> options) {
        super(options);
    }

    @NonNull
    @Override
    public DrunkRoomItemViewHolder onCreateViewHolder(View view) {
        return new DrunkRoomItemViewHolder(view);
    }

    @Override
    public int getItemLayout() {
        return R.layout.item_drunk_room;
    }

}
