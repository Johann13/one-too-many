package de.ostof.onetoomany.drunkRoom.room;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.google.firebase.firestore.Query;

import de.ostof.onetoomany.model.Debt;
import de.ostof.onetoomany.model.Drink;
import de.ostof.onetoomany.model.DrunkRoom;
import de.ostof.onetoomany.model.User;
import de.ostof.onetoomany.repo.FirestoreLiveData;
import de.ostof.onetoomany.repo.Repo;

/**
 * Created by Johann on 06.01.2018.
 */

public class DrunkRoomViewModel extends ViewModel {

    private Repo repo;

    private FirestoreLiveData firestoreLiveDataRoom;
    private MediatorLiveData<DrunkRoom> drunkRoomLiveData;

    public DrunkRoomViewModel() {
        repo = new Repo();
    }

    public LiveData<DrunkRoom> getDrunkRoom(@NonNull String id) {
        if (drunkRoomLiveData == null) {
            firestoreLiveDataRoom = new FirestoreLiveData();
            firestoreLiveDataRoom.setDocumentReference(repo.get(id));
            drunkRoomLiveData = new MediatorLiveData<>();
            drunkRoomLiveData.addSource(firestoreLiveDataRoom, documentSnapshot -> {

                if (documentSnapshot != null) {
                    drunkRoomLiveData.setValue(documentSnapshot.toObject(DrunkRoom.class));
                } else {
                    drunkRoomLiveData.setValue(null);
                }
            });
        }
        return drunkRoomLiveData;
    }

    public void deleteDrunkRoom(@NonNull DrunkRoom item,
                                @NonNull Repo.OnComplete<DrunkRoom> onComplete) {
        repo.deleteDrunkRoom(item, onComplete);
    }

    public User getUser() {
        return repo.getUser();
    }

    public Query getUsers(String roomId) {
        return repo.getUsersFromDrunkRoom(roomId);
    }

    public Query getDrinks(String id) {
        return repo.getDrinksFromDrunkRoom(id);
    }

    public void addDrink(String roomId, String name, float price, @NonNull Repo.OnComplete<Drink> onComplete) {
        repo.addDrink(roomId, name, price, onComplete);
    }

    public void addDebt(String roomId, User userBuyer, User userReceiver, Drink drink, Repo.OnComplete<Debt> onComplete) {
        repo.addDebt(roomId, userBuyer, userReceiver, drink, onComplete);
    }

    public Query getMyDebt(String drunkRoomId) {
        return repo.getMyDebts(drunkRoomId);
    }

    public Query getMyDebtsBought(String drunkRoomId) {
        return repo.getMyDebtsBought(drunkRoomId);
    }

    public Query getMyDebtsGot(String drunkRoomId) {
        return repo.getMyDebtsGot(drunkRoomId);
    }
}
