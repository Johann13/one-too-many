package de.ostof.onetoomany.drunkRoom.list;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.google.firebase.firestore.Query;

import de.ostof.onetoomany.model.DrunkRoom;
import de.ostof.onetoomany.repo.Repo;

/**
 * Created by Johann on 03.01.2018.
 */

public class DrunkRoomListViewModel extends ViewModel {

    private Repo repo;

    public DrunkRoomListViewModel() {
        repo = new Repo();
    }

    public void addDrunkRoom(@NonNull String name,
                             @NonNull Repo.OnComplete<DrunkRoom> onComplete) {
        DrunkRoom drunkRoom = new DrunkRoom();
        drunkRoom.setName(name);
        repo.addDrunkRoom(drunkRoom, onComplete);
    }

    public Query getMyDrunkRooms() {
        return repo.getMyDrunkRooms();
    }

    public void deleteDrunkRoom(@NonNull DrunkRoom item,
                                @NonNull Repo.OnComplete<DrunkRoom> onComplete) {
        repo.deleteDrunkRoom(item, onComplete);
    }
}
