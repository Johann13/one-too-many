package de.ostof.onetoomany.drunkRoom.room.drinks;

import android.support.annotation.NonNull;
import android.view.View;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import de.ostof.onetoomany.R;
import de.ostof.onetoomany.base.recyclerViewAdapter.BaseAdapter;
import de.ostof.onetoomany.model.Drink;

/**
 * Created by Johann on 15.01.2018.
 */

public class DrinkAdapter extends BaseAdapter<Drink,DrinkViewHolder> {
    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public DrinkAdapter(@NonNull FirestoreRecyclerOptions<Drink> options) {
        super(options);
    }

    @NonNull
    @Override
    public DrinkViewHolder onCreateViewHolder(View view) {
        return new DrinkViewHolder(view);
    }

    @Override
    public int getItemLayout() {
        return R.layout.item_drink;
    }
}
