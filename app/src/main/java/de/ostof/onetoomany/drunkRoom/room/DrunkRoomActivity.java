package de.ostof.onetoomany.drunkRoom.room;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ShortcutManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import java.util.Collections;

import butterknife.BindView;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.account.AccountActivity;
import de.ostof.onetoomany.account.QRScannerActivity;
import de.ostof.onetoomany.base.viewModelActivities.MenuViewModelActivity;
import de.ostof.onetoomany.drunkRoom.room.debts.DebtFragment;
import de.ostof.onetoomany.drunkRoom.room.drinks.DrinksFragment;
import de.ostof.onetoomany.drunkRoom.room.users.DrunkRoomUserFragment;
import de.ostof.onetoomany.model.DrunkRoom;

public class DrunkRoomActivity extends MenuViewModelActivity<DrunkRoomViewModel> {


    private DrunkRoom drunkRoom;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel.getDrunkRoom(
                getIntent().getStringExtra("drunkRoomId"))
                .observe(this, drunkRoom -> {
                    if (drunkRoom == null) {
                        setToolbarSubTitle(null);
                        return;
                    }
                    this.drunkRoom = drunkRoom;
                    setToolbarTitle(drunkRoom.getName());
                });
        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public String getTag() {
        return "DrunkRoomActivity";
    }

    @Override
    public int getToolbarTitle() {
        return 0;
    }

    @Override
    public int getToolbarSubTitle() {
        return 0;
    }

    @Override
    public boolean shouldShowBackButton() {
        return true;
    }

    @Override
    public void onHomePressed() {
        finish();
    }

    @Override
    protected int getMenuRes() {
        return R.menu.menu_drunk_room;
    }

    @Override
    public boolean onItemSelected(MenuItem menuItem, int id) {
        switch (id) {
            case R.id.menu_add_friend:
                addFriend();
                break;
            case R.id.menu_leave_room:
                leave();
                break;
            case R.id.menu_account:
                startActivity(new Intent(this, AccountActivity.class));
                break;
        }
        return false;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_drunk_room;
    }

    @NonNull
    @Override
    protected Class<DrunkRoomViewModel> getViewModelClass() {
        return DrunkRoomViewModel.class;
    }

    private void addFriend() {
        if (hasPermissions(Manifest.permission.CAMERA)) {
            Intent intent = new Intent(this, QRScannerActivity.class);
            intent.putExtra("roomId", drunkRoom.getId());
            startActivity(intent);
        } else {
            requestPermission(R.string.ask_for_camera, 13, Manifest.permission.CAMERA);
        }
    }

    private void leave() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Leave " + drunkRoom.getName())
                .setMessage("Do you want to leave " + drunkRoom.getName() + "?")
                .setPositiveButton("Yes leave!", (dialogInterface, i) ->
                        viewModel.deleteDrunkRoom(drunkRoom, drunkRoom -> {
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
                                ShortcutManager shortcutManager = getSystemService(ShortcutManager.class);
                                if (shortcutManager != null) {
                                    shortcutManager.removeDynamicShortcuts(Collections.singletonList(drunkRoom.getId()));
                                }
                            }
                        }))
                .setNegativeButton("No remain.", null)
                .create();
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 13) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                addFriend();
            }
        }
    }


    class FragmentAdapter extends FragmentPagerAdapter {

        public FragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new DrunkRoomUserFragment();
            } else if (position == 1) {
                return new DebtFragment();
            } else {
                return new DrinksFragment();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "Friends";
            } else if (position == 1) {
                return "My debts";
            } else {
                return "Drinks";
            }
        }
    }
}
