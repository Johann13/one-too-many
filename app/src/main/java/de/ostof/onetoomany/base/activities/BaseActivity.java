package de.ostof.onetoomany.base.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;
import timber.log.Timber;

/**
 * Created by Johann on 26.12.2017.
 * BaseActivity
 */

@SuppressWarnings("unused")
public abstract class BaseActivity extends AppCompatActivity {

    private boolean firstStart;

    public abstract String getTag();

    @LayoutRes
    protected abstract int getLayout();

    //region Lifecycle methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firstStart = savedInstanceState == null;
        logLifecycle("onCreate");
        if (getLayout() != 0) {
            setContentView(getLayout());
            bind();
        }
    }

    protected void bind() {
        ButterKnife.bind(this);
    }

    protected boolean isFirstStart() {
        return firstStart;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        logLifecycle("onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        logLifecycle("onRestoreInstanceState");
    }

    @Override
    protected void onStart() {
        super.onStart();
        logLifecycle("onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        logLifecycle("onResume");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logLifecycle("onActivityResult");
    }

    @Override
    protected void onPause() {
        super.onPause();
        logLifecycle("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        logLifecycle("onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logLifecycle("onDestroy");
    }
    //endregion

    //region Permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        logLifecycle("onRequestPermissionsResult");
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    protected boolean hasPermissions(@NonNull String... permissions) {
        return EasyPermissions.hasPermissions(this, permissions);
    }

    protected void requestPermission(@StringRes int rational, int requestCode,
                                     @NonNull String... permissions) {
        EasyPermissions.requestPermissions(this, getString(rational), requestCode, permissions);
    }

    protected void requestPermission(@StringRes int rational, int requestCode,
                                     @StringRes int positiveButton, @StringRes int negativeButton,
                                     @NonNull String... permissions) {
        PermissionRequest.Builder builder = new PermissionRequest.Builder(this, requestCode, permissions)
                .setRationale(rational)
                .setPositiveButtonText(positiveButton)
                .setNegativeButtonText(negativeButton);
        EasyPermissions.requestPermissions(builder.build());

    }
    //endregion

    //region Logging
    private void logLifecycle(String lifecycle) {
        Timber.tag(getTag() + " Lifecycle");
        Timber.d(lifecycle);
    }

    protected void logD(String msg) {
        Timber.tag(getTag());
        Timber.d(msg);
    }

    protected void logError(Exception e) {
        Timber.tag(getTag());
        Timber.e(e);
    }

    private void logError(String msg) {
        Timber.tag(getTag());
        Timber.e(msg);
    }
    //endregion

    //region Keyboard
    protected void showKeyboardSetFocus(EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        editText.requestFocus();
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(editText, 0);
        }
        /*
        hideKeyboard();
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);*/
    }

    protected void hideKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }
        getCurrentFocus().clearFocus();
        /*
        View view =  getActivity().getCurrentFocus();
        if (view != null) {
            view.setSelected(false);
            view.clearFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }*/
    }
    //endregion

}
