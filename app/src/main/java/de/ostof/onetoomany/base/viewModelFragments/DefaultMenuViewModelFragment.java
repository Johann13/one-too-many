package de.ostof.onetoomany.base.viewModelFragments;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import de.ostof.onetoomany.base.fragments.DefaultMenuFragment;


/**
 * Created by Johann on 31.08.2017.
 * Adds support for a ViewModel.
 */

public abstract class DefaultMenuViewModelFragment<VM extends ViewModel> extends DefaultMenuFragment {

    protected VM viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of((FragmentActivity) getActivity()).get(getViewModelClass());
    }

    public VM getViewModel() {
        return viewModel;
    }

    @NonNull
    protected abstract Class<VM> getViewModelClass();
}
