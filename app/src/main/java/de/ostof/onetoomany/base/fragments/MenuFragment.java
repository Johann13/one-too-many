package de.ostof.onetoomany.base.fragments;

import android.os.Bundle;
import android.support.annotation.MenuRes;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/**
 * Created by Johann on 12.07.2017.
 * Class that implements menu logic.
 */

@SuppressWarnings("unused")
public abstract class MenuFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        int res = getMenuRes();
        if (res != 0) {
            inflater.inflate(res, menu);
        } else {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return menuItemSelected(item, item.getItemId()) || super.onOptionsItemSelected(item);
    }

    @MenuRes
    public abstract int getMenuRes();

    public abstract boolean menuItemSelected(MenuItem item, int id);

}
