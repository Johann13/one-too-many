package de.ostof.onetoomany.base.recyclerViewAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by Johann on 13.07.2017.
 */

public abstract class BaseViewHolder<M>
        extends RecyclerView.ViewHolder {

    protected M item;
    protected int pos;
    protected OnItemClickListener<M> onItemClickListener;
    protected OnItemLongClickListener<M> onItemLongClickListener;
    private boolean onBind;

    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClickListener(item, pos);
                }
            }
        });

        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (onItemLongClickListener != null) {
                    onItemLongClickListener.onItemLongClickListener(item, pos);
                    return true;
                }
                return false;
            }
        });
    }

    public void bind(M item, int pos) {
        onBind = false;
        this.item = item;
        this.pos = pos;
        setView(itemView, item, pos);
        onBind = true;
    }

    protected boolean isOnBind() {
        return onBind;
    }

    protected abstract void setView(View view, M item, int pos);

    public void setOnItemClickListener(OnItemClickListener<M> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener<M> onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public M getItem() {
        return item;
    }
}
