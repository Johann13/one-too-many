package de.ostof.onetoomany.base.viewModelActivities;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import de.ostof.onetoomany.base.activities.MenuActivity;


/**
 * Created by Johann on 12.07.2017.
 * Adds support for a ViewModel.
 */

@SuppressWarnings("unused")
public abstract class MenuViewModelActivity<VM extends ViewModel> extends MenuActivity {

    protected VM viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(getViewModelClass());
    }

    protected VM getViewModel() {
        return viewModel;
    }

    @NonNull
    protected abstract Class<VM> getViewModelClass();
}
