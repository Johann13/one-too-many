package de.ostof.onetoomany.base.fragments;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import de.ostof.onetoomany.R;


/**
 * Created by Johann on 31.08.2017.
 * Class that adds the default menu of the app.
 */

public abstract class DefaultMenuFragment extends MenuFragment {

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_default, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*if (item.getItemId() == R.id.menu_feedback) {
            return true;
        } else if (item.getItemId() == R.id.menu_settings) {
            //startActivity(new Intent(getActivity(), SettingsActivity.class));
            return true;
        } else if (item.getItemId() == R.id.menu_about) {
            //startActivity(new Intent(getActivity(), AboutActivity.class));
            return true;
        } else */return super.onOptionsItemSelected(item);

    }
}
