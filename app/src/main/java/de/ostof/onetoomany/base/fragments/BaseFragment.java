package de.ostof.onetoomany.base.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;
import timber.log.Timber;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Johann on 12.07.2017.
 * Base class for all fragments.
 * Implements debug logging and butterknife logic.
 */

@SuppressWarnings("unused")
public abstract class BaseFragment extends Fragment {

    protected Unbinder unbinder;

    private boolean firstStart;

    public abstract void initView(@NonNull View view, @Nullable Bundle savedInstanceState);

    @NonNull
    public abstract String getFragmentTag();

    @LayoutRes
    protected abstract int getLayout();

    //region Lifecycle methods
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logLifecycle("onCreate");
        firstStart = savedInstanceState == null;
    }

    public boolean isFirstStart() {
        return firstStart;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        logLifecycle("onAttach");
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        logLifecycle("onActivityCreated");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        logLifecycle("onCreateView");
        if (getLayout() != 0) {
            View view = inflater.inflate(getLayout(), container, false);
            unbinder = ButterKnife.bind(this, view);
            return view;
        } else {
            return null;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        logLifecycle("onViewCreated");
        initView(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        logLifecycle("onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        logLifecycle("onResume");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logLifecycle("onActivityResult");
    }

    @Override
    public void onPause() {
        super.onPause();
        logLifecycle("onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        logLifecycle("onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        logLifecycle("onDestroyView");
        if (unbinder != null) {
            unbinder.unbind();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        logLifecycle("onDestroy");
    }
    //endregion

    //region Permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        logLifecycle("onRequestPermissionsResult");
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    protected boolean hasPermissions(@NonNull String... permissions) {
        return getActivity() != null && EasyPermissions.hasPermissions(getActivity(), permissions);
    }

    protected void requestPermission(@StringRes int rational, int requestCode,
                                     @NonNull String... permissions) {
        EasyPermissions.requestPermissions(this, getString(rational), requestCode, permissions);
    }

    protected void requestPermission(@StringRes int rational, int requestCode,
                                     @StringRes int positiveButton, @StringRes int negativeButton,
                                     @NonNull String... permissions) {
        PermissionRequest.Builder builder = new PermissionRequest.Builder(this, requestCode, permissions)
                .setRationale(rational)
                .setPositiveButtonText(positiveButton)
                .setNegativeButtonText(negativeButton);
        EasyPermissions.requestPermissions(builder.build());
    }
    //endregion

    //region Logging
    protected void logLifecycle(String lifecycle) {
        Timber.tag(getFragmentTag() + " Lifecycle");
        Timber.d(lifecycle);
    }

    protected void logD(String msg) {
        Timber.tag(getFragmentTag());
        Timber.d(msg);
    }

    protected void logError(Exception e) {
        Timber.tag(getFragmentTag());
        Timber.e(e);
    }

    protected void logError(String msg) {
        Timber.tag(getFragmentTag());
        Timber.e(msg);
    }
    //endregion

    //region Keyboard
    protected void showKeyboardSetFocus(EditText editText) {
        if (getActivity() == null) {
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        editText.requestFocus();
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(editText, 0);
        }
        /*
        hideKeyboard();
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);*/
    }

    protected void hideKeyboard() {
        if (getActivity() != null) {
            if (getActivity().getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }
            }
        }
        /*
        View view =  getActivity().getCurrentFocus();
        if (view != null) {
            view.setSelected(false);
            view.clearFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }*/
    }
    //endregion

    protected void finish() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    protected boolean isOrientationPortrait() {
        if (getActivity() == null) {
            return false;
        }

        return getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    protected boolean isOrientationLandscape() {
        if (getActivity() == null) {
            return false;
        }
        return getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

}
