package de.ostof.onetoomany.base.activities;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import de.ostof.onetoomany.R;

/**
 * Created by Johann on 31.08.2017.
 * Class that adds the default menu for this app.
 */

@SuppressWarnings("unused")
public abstract class DefaultMenuActivity extends MenuActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean r = super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_default, menu);
        return r;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*if (item.getItemId() == R.id.menu_feedback) {
            return true;
        } else if (item.getItemId() == R.id.menu_settings) {
            //startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (item.getItemId() == R.id.menu_about) {
            //startActivity(new Intent(this, AboutActivity.class));
            return true;
        } else
            */return super.onOptionsItemSelected(item);
    }

}
