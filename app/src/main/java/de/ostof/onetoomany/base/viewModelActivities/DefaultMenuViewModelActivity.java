package de.ostof.onetoomany.base.viewModelActivities;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import de.ostof.onetoomany.base.activities.DefaultMenuActivity;


/**
 * Created by Johann on 31.08.2017.
 * Adds support for a ViewModel.
 */

public abstract class DefaultMenuViewModelActivity<VM extends ViewModel> extends DefaultMenuActivity {

    protected VM viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(getViewModelClass());
    }

    protected VM getViewModel() {
        return viewModel;
    }

    @NonNull
    protected abstract Class<VM> getViewModelClass();
}
