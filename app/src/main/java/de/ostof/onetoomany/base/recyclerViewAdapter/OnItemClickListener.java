package de.ostof.onetoomany.base.recyclerViewAdapter;

/**
 * Created by Johann on 13.07.2017.
 */

public interface OnItemClickListener<M> {
    void onItemClickListener(M item, int pos);
}
