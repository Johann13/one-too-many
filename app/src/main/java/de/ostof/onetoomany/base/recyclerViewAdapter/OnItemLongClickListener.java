package de.ostof.onetoomany.base.recyclerViewAdapter;

/**
 * Created by Johann on 13.07.2017.
 */

public interface OnItemLongClickListener<M> {
    void onItemLongClickListener(M item, int pos);
}
