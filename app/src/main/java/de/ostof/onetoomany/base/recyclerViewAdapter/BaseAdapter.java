package de.ostof.onetoomany.base.recyclerViewAdapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

/**
 * Created by Johann on 04.01.2018.
 */

public abstract class BaseAdapter<M, VH extends BaseViewHolder<M>>
        extends FirestoreRecyclerAdapter<M, VH> {

    private OnItemClickListener<M> onItemClickListener;
    private OnItemLongClickListener<M> onItemLongClickListener;

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public BaseAdapter(@NonNull FirestoreRecyclerOptions<M> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull VH holder, int position, @NonNull M model) {
        holder.bind(model, position);
        holder.setOnItemClickListener(onItemClickListener);
        holder.setOnItemLongClickListener(onItemLongClickListener);
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return onCreateViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(getItemLayout(), parent, false));
    }

    @NonNull
    public abstract VH onCreateViewHolder(View view);

    @LayoutRes
    public abstract int getItemLayout();

    public void setOnItemClickListener(OnItemClickListener<M> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener<M> onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public BaseAdapter withOnItemClickListener(OnItemClickListener<M> onItemClickListener) {
        setOnItemClickListener(onItemClickListener);
        return this;
    }

    public BaseAdapter withOnItemLongClickListener(OnItemLongClickListener<M> onItemLongClickListener) {
        setOnItemLongClickListener(onItemLongClickListener);
        return this;
    }
}
