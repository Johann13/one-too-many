package de.ostof.onetoomany.base.activities;

import android.support.annotation.MenuRes;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/**
 * Created by Johann on 12.07.2017.
 * Class that implements Menu logic
 */

@SuppressWarnings("unused")
public abstract class MenuActivity extends ToolbarActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (getMenuRes() != 0) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(getMenuRes(), menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return onItemSelected(item, item.getItemId()) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onHomePressed() {
        onBackPressed();
    }

    @MenuRes
    protected abstract int getMenuRes();

    public abstract boolean onItemSelected(MenuItem menuItem, int id);
}
