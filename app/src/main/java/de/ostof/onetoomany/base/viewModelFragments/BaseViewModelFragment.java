package de.ostof.onetoomany.base.viewModelFragments;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import de.ostof.onetoomany.base.fragments.BaseFragment;


/**
 * Created by Johann on 12.07.2017.
 * Adds support for a ViewModel.
 */

@SuppressWarnings("unused")
public abstract class BaseViewModelFragment<VM extends ViewModel> extends BaseFragment {

    protected VM viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getActivity()==null){
            try {
                throw new Exception("This should not happened :(");
            } catch (Exception e) {
                e.printStackTrace();
            }
            finish();
            return;
        }
        viewModel = ViewModelProviders.of(getActivity()).get(getViewModelClass());
    }

    public VM getViewModel() {
        return viewModel;
    }

    @NonNull
    protected abstract Class<VM> getViewModelClass();
}
