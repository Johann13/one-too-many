package de.ostof.onetoomany.base.activities;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import de.ostof.onetoomany.R;
import de.ostof.onetoomany.util.DisplayUtil;

/**
 * Created by Johann on 12.07.2017.
 * Class that implements the toolbar logic.
 */

@SuppressWarnings("unused")
public abstract class ToolbarActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        setToolbarTitle(getToolbarTitle());
        setBackButton(shouldShowBackButton());
    }

    protected Toolbar getToolbar() {
        return toolbar;
    }

    //region Toolbar title
    @StringRes
    public abstract int getToolbarTitle();

    protected void setToolbarTitle(@Nullable String s) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(s);
        }
    }

    protected void setToolbarTitle(@StringRes int s) {
        if (getSupportActionBar() != null) {
            if (s != 0) {
                getSupportActionBar().setTitle(s);
            } else {
                setToolbarTitle(null);
            }
        }
    }
    //endregion

    //region Toolbar subtitle

    @StringRes
    public abstract int getToolbarSubTitle();

    public void setToolbarSubTitle(@Nullable String s) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(s);
        }
    }

    public void setToolbarSubTitle(@StringRes int s) {
        if (getSupportActionBar() != null) {
            if (s != 0) {
                getSupportActionBar().setSubtitle(s);
            } else {
                setToolbarSubTitle(null);
            }
        }
    }
    //endregion

    //region Backbutton
    public abstract boolean shouldShowBackButton();

    public void setBackButton(boolean b) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(b);
            //setToolbarIconAndTextColor(Color.BLACK);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onHomePressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public abstract void onHomePressed();
    //endregion

    //region Toolbar styling
    public void setToolbarIconAndTextColor(int color) {
        Drawable drawableO = toolbar.getOverflowIcon();
        Drawable drawableN = toolbar.getNavigationIcon();

        if (drawableO != null) {
            drawableO = DrawableCompat.wrap(drawableO);
        }

        if (drawableN != null) {
            drawableN = DrawableCompat.wrap(drawableN);
        }

        toolbar.setTitleTextColor(color);
        toolbar.setSubtitleTextColor(color);
        toolbar.setDrawingCacheBackgroundColor(color);
        if (drawableO != null) {
            DrawableCompat.setTint(drawableO, color);
            toolbar.setOverflowIcon(drawableO);
        }
        if (drawableN != null) {
            DrawableCompat.setTint(drawableN, color);
            toolbar.setNavigationIcon(drawableN);
        }
    }

    public void elevateToolbar(boolean elevate) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (elevate) {
                getToolbar().setElevation(DisplayUtil.dpToPx(4));
            } else {
                getToolbar().setElevation(0);
            }
        }
    }

    public void setToolbarElevation(float px) {
        getToolbar().setElevation(px);
    }


    public void setToolbarElevationDp(int dp) {
        getToolbar().setElevation(DisplayUtil.dpToPx(dp));
    }
    //endregion

}
