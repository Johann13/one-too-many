package de.ostof.onetoomany;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.firebase.ui.auth.AuthUI;

import butterknife.BindView;
import de.ostof.onetoomany.account.AccountActivity;
import de.ostof.onetoomany.base.viewModelActivities.MenuViewModelActivity;
import de.ostof.onetoomany.drunkRoom.list.DrunkRoomListFragment;
import de.ostof.onetoomany.model.User;
import de.ostof.onetoomany.startup.SplashActivity;

public class MainActivity extends MenuViewModelActivity<MainViewModel> {

    @BindView(R.id.frameLayoutContainer)
    FrameLayout frameLayoutContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        User user = viewModel.getUser();
        setToolbarSubTitle("Logged in as " + user.getName());
        if (isFirstStart()) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(frameLayoutContainer.getId(), new DrunkRoomListFragment())
                    .commit();

        }
    }

    @Override
    public String getTag() {
        return "MainActivity";
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }


    @Override
    public int getToolbarTitle() {
        return R.string.app_name;
    }

    @Override
    public int getToolbarSubTitle() {
        return 0;
    }

    @Override
    public boolean shouldShowBackButton() {
        return false;
    }

    @Override
    public void onHomePressed() {

    }

    @Override
    protected int getMenuRes() {
        return R.menu.menu_main;
    }

    @Override
    public boolean onItemSelected(MenuItem menuItem, int id) {
        if (id == R.id.menu_logout) {
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(task -> {
                        // user is now signed out
                        startActivity(new Intent(MainActivity.this, SplashActivity.class));
                        finish();
                    });
            return true;
        } else if (id == R.id.menu_account) {
            startActivity(new Intent(this, AccountActivity.class));
        }
        return false;
    }

    @NonNull
    @Override
    protected Class<MainViewModel> getViewModelClass() {
        return MainViewModel.class;
    }
}
