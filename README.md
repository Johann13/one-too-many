# README #

This is an app to track your and your friends drinking expenses. 
It was inspired by Tom ([his channel](https://www.youtube.com/channel/UC5rUMdCFWPXYs9e8PBLzq5g)) 
from the Yogscast ([Twitch](https://www.twitch.tv/yogscast) [Youtube](https://www.youtube.com/channel/UCH-_hzb2ILSCo9ftVSnrCIQ)) 
and was a fun experience. 

To get it running you need to create
an Firebase account (link your google account to it) and link the app to it.
This is the source code for the Android app and firebase cloud functions is used.
It is by no means perfect and is not intended to be released.